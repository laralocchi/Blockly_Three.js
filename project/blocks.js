Blockly.Blocks['initialization'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("INITIALIZATION");
    this.setNextStatement(true, null);
    this.setColour(120);
 	this.setTooltip("must be the first block");
 	this.setHelpUrl(null);
  }
};

Blockly.JavaScript['initialization'] = function(block) {
  var code = 'var canvas = document.getElementById("canv");\nvar renderer = new THREE.WebGLRenderer({canvas: canvas, alpha: true});\nrenderer.setSize(canvas.offsetWidth, canvas.offsetHeight);\n';  
  return code;
};


Blockly.Blocks['scene'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("SCENE");
    this.appendDummyInput()
    	.appendField("background")
    	.appendField(new Blockly.FieldColour("#ffffff"), "colour");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(330);
 	this.setTooltip("creates the scene");
 	this.setHelpUrl("https://threejs.org/docs/index.html#api/scenes/Scene");
  }
};

Blockly.JavaScript['scene'] = function(block) {
  var colour_colour = block.getFieldValue('colour');
  colour_colour = colour_colour.substring(1); //remove '#'
  var code = 'var scene = new THREE.Scene();\nscene.background = new THREE.Color(0x'+colour_colour+');\n';
  return code;
};


Blockly.Blocks['perspective_camera'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("PERSPECTIVE CAMERA");
    this.appendDummyInput()
    	.appendField("frustum vertical field of view")
    	.appendField(new Blockly.FieldNumber(50), "fov");
    this.appendDummyInput()
    	.appendField("frustum near plane")
    	.appendField(new Blockly.FieldNumber(0.1), "near");
    this.appendDummyInput()
    	.appendField("frustum far plane")
    	.appendField(new Blockly.FieldNumber(1000), "far");
    this.appendDummyInput()
    	.appendField("position")
    	.appendField("x")
    	.appendField(new Blockly.FieldNumber(0), "x")
    	.appendField("y")
    	.appendField(new Blockly.FieldNumber(0), "y")
    	.appendField("z")
    	.appendField(new Blockly.FieldNumber(10), "z");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(230);
 	this.setTooltip("point of view of the scene");
 	this.setHelpUrl("https://threejs.org/docs/index.html#api/cameras/PerspectiveCamera");
  }
};

Blockly.JavaScript['perspective_camera'] = function(block) {
  var number_fov = block.getFieldValue('fov');
  var number_near = block.getFieldValue('near');
  var number_far = block.getFieldValue('far');
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_z = block.getFieldValue('z');
  var code = 'var camera = new THREE.PerspectiveCamera('+number_fov+', renderer.domElement.width/renderer.domElement.height, '+number_near+', '+number_far+');\ncamera.position.set('+number_x+', '+number_y+', '+number_z+');\n';
  return code;
};


Blockly.Blocks['renderer'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("RENDERER");
    this.appendStatementInput("operations");
    this.setPreviousStatement(true, null);
    this.setColour(120);
 	this.setTooltip("render the scene");
 	this.setHelpUrl("");
  }
};

Blockly.JavaScript['renderer'] = function(block) {
  var statements_operations = Blockly.JavaScript.statementToCode(block, 'operations');
  var code = 'function animate(){\n  requestAnimationFrame(animate);\n'+ statements_operations +'  renderer.render(scene, camera);\n}\nanimate();\n';
  return code;
};
	
	
Blockly.Blocks['ambient_light'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("AMBIENT LIGHT");
    this.appendDummyInput()
    	.appendField("color")
    	.appendField(new Blockly.FieldTextInput("0xffffff"), "color");
    this.appendDummyInput()
    	.appendField("intensity")
    	.appendField(new Blockly.FieldNumber(1), "intensity");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(300);
 	this.setTooltip("creates a light (need if there is at least one model)");
 	this.setHelpUrl("https://threejs.org/docs/index.html#api/lights/AmbientLight");
  }
};

Blockly.JavaScript['ambient_light'] = function(block) {
  var text_color = block.getFieldValue('color');
  var number_intensity = block.getFieldValue('intensity');
  var code = 'var ambient_light = new THREE.AmbientLight('+text_color+', '+number_intensity+');\nscene.add(ambient_light);\n';
  return code;
};


Blockly.Blocks['cube'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("CUBE");
    this.appendDummyInput()
    	.appendField("name")
    	.appendField(new Blockly.FieldVariable(null), "name");
    this.appendDummyInput()
    	.appendField("size")
    	.appendField(new Blockly.FieldNumber(1), "size");
    this.appendDummyInput()
    	.appendField("colour")
    	.appendField(new Blockly.FieldColour("#33cc00"), "colour");
    this.appendDummyInput()
    	.appendField("position")
    	.appendField("x")
    	.appendField(new Blockly.FieldNumber(0), "posX")
        .appendField("y")
        .appendField(new Blockly.FieldNumber(0), "posY")
        .appendField("z")
        .appendField(new Blockly.FieldNumber(0), "posZ");
   		this.setPreviousStatement(true, null);
    	this.setNextStatement(true, null);
    	this.setColour(0);
 		this.setTooltip("creates a cube in the scene");
 		this.setHelpUrl("");
  }
};

Blockly.JavaScript['cube'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var number_size = block.getFieldValue('size');
  var colour_colour = block.getFieldValue('colour');
  var number_posx = block.getFieldValue('posX');
  var number_posy = block.getFieldValue('posY');
  var number_posz = block.getFieldValue('posZ');
  colour_colour = colour_colour.substring(1); //remove '#'
  var code = variable_name+' = new THREE.Mesh(new THREE.BoxGeometry('+number_size+', '+number_size+', '+number_size+'),new THREE.MeshBasicMaterial({color: 0x'+colour_colour+'}));\nscene.add('+variable_name+');\n';
  if(number_posx != "0" || number_posy != "0" || number_posz != "0")
  	code += variable_name+'.position.set('+number_posx+', '+number_posy+', '+number_posz+');\n';
  return code;
};


Blockly.Blocks['sphere'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("SPHERE");
    this.appendDummyInput()
        .appendField("name")
        .appendField(new Blockly.FieldVariable(null), "name");
    this.appendDummyInput()
        .appendField("size")
        .appendField(new Blockly.FieldNumber(1), "size");
    this.appendDummyInput()
        .appendField("colour")
        .appendField(new Blockly.FieldColour("#ff0000"), "colour");
    this.appendDummyInput()
        .appendField("position")
        .appendField("x")
        .appendField(new Blockly.FieldNumber(0), "posX")
        .appendField("y")
        .appendField(new Blockly.FieldNumber(0), "posY")
        .appendField("z")
        .appendField(new Blockly.FieldNumber(0), "posZ");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 	this.setTooltip("creates a sphere in the scene");
 	this.setHelpUrl("");
  }
};

Blockly.JavaScript['sphere'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var number_size = block.getFieldValue('size');
  var colour_colour = block.getFieldValue('colour');
  colour_colour = colour_colour.substring(1);﻿//remove '#'
  var number_posx = block.getFieldValue('posX');
  var number_posy = block.getFieldValue('posY');
  var number_posz = block.getFieldValue('posZ');
  var code = variable_name+' = new THREE.Mesh(new THREE.SphereGeometry('+number_size+', 30, 30),new THREE.MeshBasicMaterial({color: 0x'+colour_colour
  +'}));\nscene.add('+variable_name+');\n';
  if(number_posx != "0" || number_posy != "0" || number_posz != "0")
  	code += variable_name+'.position.set('+number_posx+', '+number_posy+', '+number_posz+');\n';
  return code;
};


Blockly.Blocks['json_loader'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("LOADER");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 	this.setTooltip("creates a model loader (only one is needed)");
 	this.setHelpUrl("https://threejs.org/docs/index.html#api/loaders/JSONLoader");
  }
};

Blockly.JavaScript['json_loader'] = function(block) {
  var code = 'var loader = new THREE.JSONLoader();\n';
  return code;
};


Blockly.Blocks['model'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("MODEL");
    this.appendDummyInput()
        .appendField("name")
        .appendField(new Blockly.FieldVariable(null), "name");
    this.appendDummyInput()
        .appendField("path")
        .appendField(new Blockly.FieldTextInput("models/..."), "path");
    this.appendDummyInput()
        .appendField("position")
        .appendField("x")
        .appendField(new Blockly.FieldNumber(0), "posX")
        .appendField("y")
        .appendField(new Blockly.FieldNumber(0), "posY")
        .appendField("z")
        .appendField(new Blockly.FieldNumber(0), "posZ");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 	this.setTooltip("imports the JSON model specified");
 	this.setHelpUrl("https://threejs.org/docs/index.html#api/loaders/JSONLoader");
  }
};

Blockly.JavaScript['model'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var text_path = block.getFieldValue('path');
  var number_posx = block.getFieldValue('posX');
  var number_posy = block.getFieldValue('posY');
  var number_posz = block.getFieldValue('posZ');
  text_path = "'"+text_path+"'";
  var code = variable_name+' = new THREE.Object3D();\nscene.add('+variable_name+');\nloader.load('+text_path+',\n  function (geometry, materials){\n    var object = new THREE.Mesh(geometry, materials);\n    '+variable_name+'.add(object);\n  }\n);\n';
  if(number_posx != "0" || number_posy != "0" || number_posz != "0")
  	code += variable_name+'.position.set('+number_posx+', '+number_posy+', '+number_posz+');\n';
  return code;
};


Blockly.Blocks['scale'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("SCALE");
    this.appendDummyInput()
    	.appendField(new Blockly.FieldVariable(null), "name");
    this.appendDummyInput()
    	.appendField("x")
    	.appendField(new Blockly.FieldNumber(1), "x");
    this.appendDummyInput()
    	.appendField("y")
    	.appendField(new Blockly.FieldNumber(1), "y");
    this.appendDummyInput()
    	.appendField("z")
    	.appendField(new Blockly.FieldNumber(1), "z");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 	this.setTooltip("scale the object specified");
 	this.setHelpUrl("");
  }
};

Blockly.JavaScript['scale'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_z = block.getFieldValue('z');
  var code = variable_name+'.scale.set('+number_x+', '+number_y+', '+number_z+');\n';
  return code;
};


Blockly.Blocks['rotation'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("ROTATION");
    this.appendDummyInput()
    	.appendField(new Blockly.FieldVariable(null), "name");
    this.appendDummyInput()
    	.appendField("x")
    	.appendField(new Blockly.FieldNumber(0), "x");
    this.appendDummyInput()
    	.appendField("y")
    	.appendField(new Blockly.FieldNumber(0), "y");
    this.appendDummyInput()
    	.appendField("z")
    	.appendField(new Blockly.FieldNumber(0), "z");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 	this.setTooltip("rotates the object specified");
 	this.setHelpUrl("");
  }
};

Blockly.JavaScript['rotation'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_z = block.getFieldValue('z');
  var code = variable_name+'.rotation.set('+number_x+', '+number_y+', '+number_z+');\n';
  return code;
};


Blockly.Blocks['position'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("POSITION");
    this.appendDummyInput()
    	.appendField(new Blockly.FieldVariable(null), "name");
    this.appendDummyInput()
    	.appendField("x")
    	.appendField(new Blockly.FieldNumber(0), "x");
    this.appendDummyInput()
    	.appendField("y")
    	.appendField(new Blockly.FieldNumber(0), "y");
    this.appendDummyInput()
    	.appendField("z")
    	.appendField(new Blockly.FieldNumber(0), "z");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(0);
 	this.setTooltip("repositions the object specified");
 	this.setHelpUrl("");
  }
};

Blockly.JavaScript['position'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_z = block.getFieldValue('z');
  var code = variable_name+'.position.set('+number_x+', '+number_y+', '+number_z+');\n';
  return code;
};


Blockly.Blocks['group_item'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("ITEM")
    	.appendField(new Blockly.FieldVariable(null), "name");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(60);
 	this.setTooltip("must be insert in the group block");
 	this.setHelpUrl("https://threejs.org/docs/index.html#api/objects/Group");
  }
};

Blockly.JavaScript['group_item'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var code = variable_name+', ';
  return code;
};

    
Blockly.Blocks['group'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("GROUP");
    this.appendDummyInput()
    	.appendField("name")
    	.appendField(new Blockly.FieldVariable(null), "name");
    this.appendStatementInput("items");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(60);
 	this.setTooltip("creates a group of objects");
 	this.setHelpUrl("https://threejs.org/docs/index.html#api/objects/Group");
  }
};

Blockly.JavaScript['group'] = function(block) {
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var statements_items = Blockly.JavaScript.statementToCode(block, 'items').trim();
  var code = variable_name+' = new THREE.Group();\n'+variable_name+'.add('+statements_items;
  code = code.substring(0, code.length - 1);
  code += ');\n'+'scene.add('+variable_name+');\n';
  return code;
};


Blockly.Blocks['animate'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("ANIMATE")
        .appendField(new Blockly.FieldDropdown([["ROTATION","rotation"], ["POSITION","position"], ["SCALE","scale"]]), "animate");
    this.appendDummyInput()
        .appendField(new Blockly.FieldVariable(null), "name")
        .appendField("x")
        .appendField(new Blockly.FieldNumber(0), "x")
        .appendField("y")
        .appendField(new Blockly.FieldNumber(0), "y")
        .appendField("z")
        .appendField(new Blockly.FieldNumber(0), "z");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(180);
 	this.setTooltip("animates the specified object");
 	this.setHelpUrl("");
  }
};

Blockly.JavaScript['animate'] = function(block) {
  var dropdown_animate = block.getFieldValue('animate');
  var variable_name = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('name'), Blockly.Variables.NAME_TYPE);
  var number_x = block.getFieldValue('x');
  var number_y = block.getFieldValue('y');
  var number_z = block.getFieldValue('z');
  var code = "";
  if(number_x != 0)
  	code += variable_name+'.'+dropdown_animate+'.x += '+number_x+';\n';
  if(number_y != 0)
  	code += variable_name+'.'+dropdown_animate+'.y += '+number_y+';\n';
  if(number_z != 0)
  	code += variable_name+'.'+dropdown_animate+'.z += '+number_z+';\n';
  return code;
};


Blockly.Blocks['camera_controls'] = {
  init: function() {
    this.appendDummyInput()
    	.appendField("CAMERA CONTROLS");
    this.appendDummyInput()
    	.appendField("camera zoom")
    	.appendField(new Blockly.FieldCheckbox("TRUE"), "zoom");
    this.appendDummyInput()
    	.appendField("camera rotation")
    	.appendField(new Blockly.FieldCheckbox("TRUE"), "rotate");
    this.appendDummyInput()
    	.appendField("camera panning")
    	.appendField(new Blockly.FieldCheckbox("TRUE"), "pan")
    	.appendField("keyboard ")
    	.appendField(new Blockly.FieldCheckbox("TRUE"), "keys");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
 	this.setTooltip("controls the camera with mouse and keyboard");
 	this.setHelpUrl("https://threejs.org/docs/index.html#examples/controls/OrbitControls");
  }
};

Blockly.JavaScript['camera_controls'] = function(block) {
  var checkbox_zoom = block.getFieldValue('zoom') == 'TRUE';
  var checkbox_rotate = block.getFieldValue('rotate') == 'TRUE';
  var checkbox_pan = block.getFieldValue('pan') == 'TRUE';
  var checkbox_keys = block.getFieldValue('keys') == 'TRUE';
  
  var code = 'var controls = new THREE.OrbitControls(camera,canvas);\n';
  if(checkbox_zoom == true) 
  	code += 'controls.enableZoom = true;\n'
  else
  	code += 'controls.enableZoom = false;\n'
  if(checkbox_rotate == true)
  	code += 'controls.enableRotate = true;\n'
  else
  	code += 'controls.enableRotate = false;\n'
  if(checkbox_pan == true)
  	code += 'controls.enablePan = true;\n'
  else
  	code += 'controls.enablePan = false;\n'
  if(checkbox_keys == true && checkbox_pan == true)
  	code += 'controls.enableKeys = true;\n'
  else{
  	code += 'controls.enableKeys = false;\n'
  	block.setFieldValue('FALSE','keys');
  }
  code += 'controls.update();\n';
  return code;
};

